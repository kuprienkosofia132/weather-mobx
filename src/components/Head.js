import { observer } from 'mobx-react-lite';
import { format } from 'date-fns';
import ruLocale from 'date-fns/locale/ru';
import { store } from '../lib/mobx';
import { useWeatherData } from '../hooks/useWeatherData';


export const Head = observer(() => {
    const { selectedDayId } = store;

    const data = useWeatherData();

    const choseDay = Array.isArray(data) && data.filter((day) => selectedDayId === day.id);

    return (

        <div className = 'head'>
            { Array.isArray(choseDay) && choseDay.length
                ? <>
                    <div className = { `icon ${choseDay[ 0 ].type}` }></div>
                    <div className = 'current-date'>
                        <p>{ format(new Date(choseDay[ 0 ]?.day), 'EEEE', { locale: ruLocale }) }</p>
                        <span>{ format(new Date(choseDay[ 0 ]?.day), 'd MMMM', { locale: ruLocale }) }</span>
                    </div>
                </> : null
            }
        </div>);
});
