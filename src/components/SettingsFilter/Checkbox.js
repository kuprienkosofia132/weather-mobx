export const Checkbox = (props) => {
    return (
        <span
            onClick = { () => {
                if (!props.isFiltered) {
                    props.setValue('type', props.typeName);
                    props.setType(props.typeName);
                }
            } }
            className = { `checkbox ${props.typeName === props.typeValue && 'selected'}` }>{ props.text }</span>);
};
