import 'react-toastify/dist/ReactToastify.css';

export const SettingTemperature = (props) => {
    return (
        <>
            <p className = 'custom-input'>
                <label htlmfor = { props.temperaturelevel }>{ props.label }</label>
                <input
                    disabled = { props.isFiltered }
                    name = { props.name }
                    id = { props.temperaturelevel } type = 'number'
                    { ...props.register }
                    onChange = { (event) => {
                        props.setTemp(event.target.value);
                    } } />
            </p>
        </>
    );
};
