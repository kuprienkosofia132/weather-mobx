import { useForm } from 'react-hook-form';
import { observer } from 'mobx-react-lite';
import { useEffect } from 'react';
import { toast, ToastContainer } from 'react-toastify';
import { Checkbox } from './Checkbox';
import { SettingTemperature } from './Temperature';
import { store } from '../../lib/mobx';

export const SettingsFilter = observer(() => {
    const {
        setMaxTemperature, setType, setMinTemperature,
        isFormBlocked, applyFilter, resetFilter, isFiltered, type,
    } = store;

    const {
        register, getValues, setValue, formState: { errors },
        handleSubmit, reset,
    } = useForm(
        {
            defaultValues: {
                type:           '',
                minTemperature: '',
                maxTemperature: '',
            },
        },
    );

    useEffect(() => {
        for (const key in errors) {
            if (errors[ key ] && errors[ key ]?.message) {
                toast.error(errors[ key ]?.message, {
                    position:        'top-left',
                    toastId:         name,
                    autoClose:       5000,
                    hideProgressBar: false,
                    closeOnClick:    true,
                    pauseOnHover:    true,
                    draggable:       true,
                    progress:        undefined,

                }); break;
            }
        }
    }, [errors]);

    const onSubmit = handleSubmit((dataForm) => {
        applyFilter(dataForm);
    });

    const resetForm = (event) => {
        event.preventDefault();
        reset();
        resetFilter();
    };

    return (
        <>
            <ToastContainer
                newestOnTop = { false }
                rtl = { false }
                pauseOnFocusLoss />

            <form
                className = 'filter' onSubmit = { onSubmit }>

                <Checkbox
                    setType = { setType }
                    typeValue = { type }
                    isFiltered = { isFiltered }
                    text = 'Облачно' setValue = { setValue }
                    typeName = 'cloudy' />
                <Checkbox
                    setType = { setType }
                    typeValue = { type }
                    isFiltered = { isFiltered }
                    text = 'Солнечно' setValue = { setValue }
                    typeName = 'sunny' />

                <SettingTemperature
                    setTemp = { setMinTemperature }
                    temperaturelevel = 'min-temperature'
                    label = 'Минимальная температура'
                    type = 'number'
                    isFiltered = { isFiltered }
                    name = 'minTemperature'
                    errors = { errors }
                    register = { register('minTemperature', {
                        min: -273,
                        max: 100,
                    }) } />
                <SettingTemperature
                    setTemp = { setMaxTemperature }
                    temperaturelevel = 'max-temperature'
                    label = 'Максимальная температура'
                    type = 'number'
                    isFiltered = { isFiltered }
                    name = 'maxTemperature'
                    errors = { errors }
                    register = { register('maxTemperature', {
                        min:      -273,
                        max:      100,
                        validate: (v) => {
                            const minTemp = getValues('minTemperature');

                            if (v && minTemp && minTemp > v) {
                                return 'Минимальная температура не может быть больше максималной!';
                            }

                            return true;
                        },
                    })
                    } />

                { isFiltered ?  <button
                    onClick = { resetForm }
                    type = 'button'>Сбросить</button>
                    : <button type = 'submit' disabled = { isFormBlocked }>Отфильтровать</button> }
            </form>
        </>
    );
});
