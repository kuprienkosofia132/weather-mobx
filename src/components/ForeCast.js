import ruLocale from 'date-fns/locale/ru';
import { format } from 'date-fns';
import { observer } from 'mobx-react-lite';
import { useEffect, useState } from 'react';
import { useWeatherData } from '../hooks/useWeatherData';
import { store } from '../lib/mobx';


export const ForeCast = observer(() => {
    const {
        selectedDayId, setSelectedDayId, isFiltered,
    } = store;

    const [days, setDays] = useState([]);

    const handleOnClick = (id) => {
        setSelectedDayId(id);
    };

    const data = useWeatherData();

    const { filteredDays } = store;

    useEffect(() => {
        if (Array.isArray(data)) {
            if (!isFiltered) {
                setDays(data);
            } else {
                setDays(filteredDays);
            }
        }
    }, [isFiltered, data]);


    const daysInfo = days.slice(0, 7).map(({
        id, day, temperature, type,
    }) => {
        return (
            <div
                onClick = { () => handleOnClick(id, type) } className = { ` day ${type} ${selectedDayId === id && 'selected'}` }
                key = { id }>
                <p>{ format(new Date(day), 'EEEE', { locale: ruLocale }) }</p>
                <span>{ temperature }</span>
            </div>);
    });

    useEffect(() => {
        const filterDays = Array.isArray(data) ? filteredDays(data) : null;
        if (Array.isArray(filterDays) && filterDays.length) {
            setSelectedDayId(filterDays[ 0 ].id);
        } else if (Array.isArray(filterDays) && !filterDays.length) {
            setSelectedDayId(null);
        }
    }, [days]);

    return (
        <div className = 'forecast'>
            { !selectedDayId ? <p className = 'message'>По заданым критериям нет доступных дней </p> : daysInfo }
        </div>
    );
});
