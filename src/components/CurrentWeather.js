import { observer } from 'mobx-react-lite';
import { store } from '../lib/mobx';
import { useWeatherData } from '../hooks/useWeatherData';

export const CurrentWeather = observer(() => {
    const data = useWeatherData();
    const { selectedDayId } = store;

    const choseDay = Array.isArray(data) && data.filter((day) => selectedDayId === day.id);

    return (<div className = 'current-weather'>
        { Array.isArray(choseDay) && choseDay.length
            ? <>
                <p className = 'temperature'>{ choseDay[ 0 ].temperature }</p>
                <p className = 'meta'>
                    <span className = 'rainy'>{ choseDay[ 0 ].rain_probability }</span>
                    <span className = 'humidity'>{ choseDay[ 0 ].humidity }</span>
                </p>
            </> : null }
    </div>);
});
