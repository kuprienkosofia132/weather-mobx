// Components
import { observer } from 'mobx-react-lite';
import { SettingsFilter } from './components/SettingsFilter/SetingsFilter';
import { Head } from './components/Head';
import { CurrentWeather } from './components/CurrentWeather';
import { ForeCast } from './components/ForeCast';
import { store } from './lib/mobx';

// Instruments

export const App = observer(() => {
    const { selectedDayId } = store;

    return (
        <main>
            <SettingsFilter />
            { selectedDayId !== null && <> <Head />
                <CurrentWeather /> </> }
            <ForeCast />
        </main>
    );
});

