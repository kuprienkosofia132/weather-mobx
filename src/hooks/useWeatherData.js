import { useQuery } from 'react-query';
import { api } from '../api';


export const useWeatherData = () => {
    const { data } = useQuery('data', api.getWeather);

    return (
        data
    );
};
